# Desarrollo Aplicaciones Web III



## Materiales

[Libro Ingeniería del Software](https://drive.google.com/file/d/1bbYmyyOccTJ9NsgwR6xe1CAFtRxUX4Cb/view?usp=sharing)

## Libros

[1. Introduccion a Laravel](https://drive.google.com/file/d/1CorC8saY_8aZUX_6PNP1b3xMGY0mzRT2/view?usp=sharing)

## Realizando muestro primer CRUD

[1. CRUD](CRUD/crud.md)

## Integrando template AdminLTE en laravel

[2. Template AdminLTE](Template/template.md)

## Creando nuestro proyecto

[3. Primer CRUD del proyecto](CRUD-Proyecto/crud_proyecto.md)

## Relaciones entre modelos

[4. Relaciones avanzadas entre tablas en Laravel](https://kinsta.com/es/blog/laravel-relaciones/)

[5. Práctica de relaciones en Laravel](/Relacione_Laravel/relaciones_laravel.md)

[6. Modelos](/Relacione_Laravel/modelos.md)

[7. Modelo Tablas](/Relacione_Laravel/modelos_tablas.md)

[8. Controladores](/Relacione_Laravel/controladores.md)

[9. Controladores Tablas](/Relacione_Laravel/controladores_tablas.md)


## Vistas

[Vistas de los modelos](/Relacione_Laravel/vistas.md)

## Retroalimentación

[Desarrollo de CRUD's - Usuario](/Retroalimentacion/1.Usuario.md)

[Desarrollo de CRUD's - Pais](/Retroalimentacion/2.Pais.md)