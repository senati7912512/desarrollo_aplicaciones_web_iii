# Modelos

## Uno a Uno: Usuario y Perfil

```php:
// Usuario.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = ['nombre', 'email'];

    public function perfil()
    {
        return $this->hasOne(Perfil::class);
    }
}

// Perfil.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $fillable = ['bio'];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }
}
```

## Uno a Muchos: Propietario y Departamento

```php:
// Propietario.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Propietario extends Model
{
    protected $fillable = ['nombre', 'email'];

    public function departamentos()
    {
        return $this->hasMany(Departamento::class);
    }
}

// Departamento.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $fillable = ['direccion', 'renta'];

    public function propietario()
    {
        return $this->belongsTo(Propietario::class);
    }
}

// Propietario.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Propietario extends Model
{
    protected $fillable = ['nombre', 'email'];

    public function departamentos()
    {
        return $this->hasMany(Departamento::class);
    }
}

// Departamento.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $fillable = ['direccion', 'renta'];

    public function propietario()
    {
        return $this->belongsTo(Propietario::class);
    }
}
```

## Muchos a Muchos: Inquilinos y Servicios

```php:
// Inquilino.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inquilino extends Model
{
    protected $fillable = ['nombre', 'email'];

    public function servicios()
    {
        return $this->belongsToMany(Servicio::class, 'inquilino_servicio');
    }
}

// Servicio.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $fillable = ['nombre', 'precio'];

    public function inquilinos()
    {
        return $this->belongsToMany(Inquilino::class, 'inquilino_servicio');
    }
}
```

## Has Many Through: Países y Pagos a través de Usuarios

```php:
// Pais.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $fillable = ['nombre'];

    public function usuarios()
    {
        return $this->hasMany(Usuario::class);
    }

    public function pagos()
    {
        return $this->hasManyThrough(Pago::class, Usuario::class);
    }
}

// Pago.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $fillable = ['monto', 'fecha_pago'];
}
```

## Polimórficas: Fotos pueden pertenecer tanto a Departamentos como a Inquilinos

```php:
// Foto.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $fillable = ['url', 'imageable_id', 'imageable_type'];

    public function imageable()
    {
        return $this->morphTo();
    }
}

// Departamento.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $fillable = ['direccion', 'renta'];

    public function fotos()
    {
        return $this->morphMany(Foto::class, 'imageable');
    }
}

// Inquilino.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inquilino extends Model
{
    protected $fillable = ['nombre', 'email'];

    public function fotos()
    {
        return $this->morphMany(Foto::class, 'imageable');
    }
}
```