# Relaciones en Laravel

En Laravel, las relaciones entre modelos nos permiten establecer asociaciones entre diferentes tablas de la base de datos. Aquí vamos a explorar cinco tipos comunes de relaciones:

## 1. Uno a Uno

En una relación uno a uno, un registro de una tabla está asociado con exactamente un registro de otra tabla. Por ejemplo, en un sistema de usuarios, un usuario puede tener un perfil.

### Ejemplo: Usuario y Perfil

[Diagrama entidad relacion](/Relacione_Laravel/DB_1.png)

[Orden de migraciones](/Relacione_Laravel/tabla.png)

```php:
// Migración de Usuarios
Schema::create('usuarios', function (Blueprint $table) {
    $table->id();
    $table->string('nombre');
    $table->string('email')->unique();
    $table->timestamps();
});

// Migración de Perfiles
Schema::create('perfiles', function (Blueprint $table) {
    $table->id();
    $table->string('bio');
    $table->foreignId('usuario_id')->constrained()->onDelete('cascade');
    $table->timestamps();
});

// Modelo Usuario
public function perfil() {
    return $this->hasOne(Perfil::class);
}

// Modelo Perfil
public function usuario() {
    return $this->belongsTo(Usuario::class);
}
```

## 2. Uno a Muchos

En una relación uno a muchos, un registro de una tabla está asociado con varios registros de otra tabla. Por ejemplo, un propietario puede tener varios departamentos.

### Ejemplo: Propietario y Departamento

```php:
// Migración de Propietarios
Schema::create('propietarios', function (Blueprint $table) {
    $table->id();
    $table->string('nombre');
    $table->string('email')->unique();
    $table->timestamps();
});

// Migración de Departamentos
Schema::create('departamentos', function (Blueprint $table) {
    $table->id();
    $table->string('direccion');
    $table->decimal('renta', 8, 2);
    $table->foreignId('propietario_id')->constrained()->onDelete('cascade');
    $table->timestamps();
});

// Modelo Propietario
public function departamentos() {
    return $this->hasMany(Departamento::class);
}

// Modelo Departamento
public function propietario() {
    return $this->belongsTo(Propietario::class);
}
```

## 3. Muchos a Muchos

En una relación muchos a muchos, varios registros de una tabla pueden estar asociados con varios registros de otra tabla. Por ejemplo, varios inquilinos pueden tener varios servicios.

### Ejemplo: Inquilinos y Servicios

```php:
// Migración de Inquilinos
Schema::create('inquilinos', function (Blueprint $table) {
    $table->id();
    $table->string('nombre');
    $table->string('email')->unique();
    $table->timestamps();
});

// Migración de Servicios
Schema::create('servicios', function (Blueprint $table) {
    $table->id();
    $table->string('nombre');
    $table->decimal('precio', 8, 2);
    $table->timestamps();
});

// Tabla Pivot Inquilino-Servicio
Schema::create('inquilino_servicio', function (Blueprint $table) {
    $table->id();
    $table->foreignId('inquilino_id')->constrained()->onDelete('cascade');
    $table->foreignId('servicio_id')->constrained()->onDelete('cascade');
    $table->timestamps();
});

// Modelo Inquilino
public function servicios() {
    return $this->belongsToMany(Servicio::class, 'inquilino_servicio');
}

// Modelo Servicio
public function inquilinos() {
    return $this->belongsToMany(Inquilino::class, 'inquilino_servicio');
}
```

## 4. Has Many Through

En una relación "Has Many Through", se pueden establecer relaciones entre dos modelos a través de otro modelo intermedio. Por ejemplo, un país puede tener muchos pagos a través de usuarios.

### Ejemplo: Países y Pagos a través de Usuarios

```php:
// Migración de Países
Schema::create('paises', function (Blueprint $table) {
    $table->id();
    $table->string('nombre');
    $table->timestamps();
});

// Migración de Usuarios (Actualizada con clave foránea de país)
Schema::create('usuarios', function (Blueprint $table) {
    $table->id();
    $table->string('nombre');
    $table->string('email')->unique();
    $table->foreignId('pais_id')->nullable()->constrained()->onDelete('cascade');
    $table->timestamps();
});

// Migración de Pagos
Schema::create('pagos', function (Blueprint $table) {
    $table->id();
    $table->foreignId('renta_id')->constrained()->onDelete('cascade');
    $table->foreignId('usuario_id')->constrained()->onDelete('cascade');
    $table->decimal('monto', 8, 2);
    $table->date('fecha_pago');
    $table->timestamps();
});

// Modelo País
public function pagos() {
    return $this->hasManyThrough(Pago::class, Usuario::class);
}
```

## 5. Relaciones Polimórficas

Las relaciones polimórficas permiten que un modelo pueda pertenecer a más de un tipo de modelo en una única asociación. Por ejemplo, las fotos pueden pertenecer tanto a departamentos como a inquilinos.

### Ejemplo: Fotos pueden pertenecer tanto a Departamentos como a Inquilinos

```php:
// Migración de Fotos
Schema::create('fotos', function (Blueprint $table) {
    $table->id();
    $table->string('url');
    $table->morphs('imageable');
    $table->timestamps();
});

// Modelo Departamento
public function fotos() {
    return $this->morphMany(Foto::class, 'imageable');
}

// Modelo Inquilino
public function fotos() {
    return $this->morphMany(Foto::class, 'imageable');
}

// Modelo Foto
public function imageable() {
    return $this->morphTo();
}
```
