# Controladores

## Uno a Uno: Usuario y Perfil

use App\Models\Usuario;
use App\Models\Perfil;
use Illuminate\Http\Request;

```php:
class UsuarioController extends Controller
{
    public function index()
    {
        $usuarios = Usuario::with('perfil')->get();
        return view('usuarios.index', compact('usuarios'));
    }

    public function create()
    {
        return view('usuarios.create');
    }

    public function store(Request $request)
    {
        $usuario = Usuario::create($request->all());
        $usuario->perfil()->create($request->only('bio'));
        return redirect()->route('usuarios.index')->with('success', 'Usuario creado exitosamente');
    }

    public function show($id)
    {
        $usuario = Usuario::findOrFail($id);
        return view('usuarios.show', compact('usuario'));
    }

    public function edit($id)
    {
        $usuario = Usuario::findOrFail($id);
        return view('usuarios.edit', compact('usuario'));
    }

    public function update(Request $request, $id)
    {
        $usuario = Usuario::findOrFail($id);
        $usuario->update($request->all());
        $usuario->perfil()->update($request->only('bio'));
        return redirect()->route('usuarios.index')->with('success', 'Usuario actualizado exitosamente');
    }

    public function destroy($id)
    {
        $usuario = Usuario::findOrFail($id);
        $usuario->perfil()->delete();
        $usuario->delete();
        return redirect()->route('usuarios.index')->with('success', 'Usuario eliminado exitosamente');
    }
}
```

## Uno a Muchos: Propietario y Departamento

```php:
use App\Models\Propietario;
use App\Models\Departamento;
use Illuminate\Http\Request;

class PropietarioController extends Controller
{
    public function index()
    {
        $propietarios = Propietario::with('departamentos')->get();
        return view('propietarios.index', compact('propietarios'));
    }

    public function create()
    {
        return view('propietarios.create');
    }

    public function store(Request $request)
    {
        $propietario = Propietario::create($request->all());
        $propietario->departamentos()->createMany($request->departamentos);
        return redirect()->route('propietarios.index')->with('success', 'Propietario creado exitosamente');
    }

    public function show($id)
    {
        $propietario = Propietario::findOrFail($id);
        return view('propietarios.show', compact('propietario'));
    }

    public function edit($id)
    {
        $propietario = Propietario::findOrFail($id);
        return view('propietarios.edit', compact('propietario'));
    }

    public function update(Request $request, $id)
    {
        $propietario = Propietario::findOrFail($id);
        $propietario->update($request->all());
        foreach ($request->departamentos as $departamento) {
            Departamento::updateOrCreate(['id' => $departamento['id']], $departamento);
        }
        return redirect()->route('propietarios.index')->with('success', 'Propietario actualizado exitosamente');
    }

    public function destroy($id)
    {
        $propietario = Propietario::findOrFail($id);
        $propietario->departamentos()->delete();
        $propietario->delete();
        return redirect()->route('propietarios.index')->with('success', 'Propietario eliminado exitosamente');
    }
}
```

## Muchos a Muchos: Inquilinos y Servicios

```php:
use App\Models\Inquilino;
use App\Models\Servicio;
use Illuminate\Http\Request;

class InquilinoController extends Controller
{
    public function index()
    {
        $inquilinos = Inquilino::with('servicios')->get();
        return view('inquilinos.index', compact('inquilinos'));
    }

    public function create()
    {
        $servicios = Servicio::all();
        return view('inquilinos.create', compact('servicios'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'email' => 'required|email|unique:inquilinos,email',
            'servicios' => 'required|array',
            'servicios.*' => 'exists:servicios,id',
        ]);

        $inquilino = Inquilino::create($request->only('nombre', 'email'));

        if ($request->has('servicios')) {
            $inquilino->servicios()->attach($request->servicios);
        }

        return redirect()->route('inquilinos.index')->with('success', 'Inquilino creado exitosamente');
    }

    public function show($id)
    {
        $inquilino = Inquilino::findOrFail($id);
        return view('inquilinos.show', compact('inquilino'));
    }

    public function edit($id)
    {
        $inquilino = Inquilino::findOrFail($id);
        $servicios = Servicio::all();
        return view('inquilinos.edit', compact('inquilino', 'servicios'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'email' => 'required|email|unique:inquilinos,email,' . $id,
            'servicios' => 'required|array',
            'servicios.*' => 'exists:servicios,id',
        ]);

        $inquilino = Inquilino::findOrFail($id);
        $inquilino->update($request->only('nombre', 'email'));

        $inquilino->servicios()->sync($request->servicios);

        return redirect()->route('inquilinos.index')->with('success', 'Inquilino actualizado exitosamente');
    }

    public function destroy($id)
    {
        $inquilino = Inquilino::findOrFail($id);
        $inquilino->servicios()->detach();
        $inquilino->delete();
        return redirect()->route('inquilinos.index')->with('success', 'Inquilino eliminado exitosamente');
    }
}
```

## Has Many Through: Países y Pagos a través de Usuarios

```php:
use App\Models\Pais;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    public function index()
    {
        $paises = Pais::with('pagos')->get();
        return view('paises.index', compact('paises'));
    }

    public function create()
    {
        return view('paises.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255|unique:paises,nombre',
        ]);

        $pais = Pais::create($request->only('nombre'));

        return redirect()->route('paises.index')->with('success', 'País creado exitosamente');
    }

    public function show($id)
    {
        $pais = Pais::findOrFail($id);
        return view('paises.show', compact('pais'));
    }

    public function edit($id)
    {
        $pais = Pais::findOrFail($id);
        return view('paises.edit', compact('pais'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255|unique:paises,nombre,' . $id,
        ]);

        $pais = Pais::findOrFail($id);
        $pais->update($request->only('nombre'));

        return redirect()->route('paises.index')->with('success', 'País actualizado exitosamente');
    }

    public function destroy($id)
    {
        $pais = Pais::findOrFail($id);
        $pais->delete();
        return redirect()->route('paises.index')->with('success', 'País eliminado exitosamente');
    }
}
```

## Polimórficas: Fotos pueden pertenecer tanto a Departamentos como a Inquilinos

```php:
use App\Models\Foto;
use App\Models\Departamento;
use App\Models\Inquilino;
use Illuminate\Http\Request;

class FotoController extends Controller
{
    public function create()
    {
        return view('fotos.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tipo' => 'required|string|in:departamento,inquilino',
            'modelo_id' => 'required|integer',
            'url' => 'required|string|max:255',
        ]);

        if ($request->tipo === 'departamento') {
            $modelo = Departamento::findOrFail($request->modelo_id);
        } elseif ($request->tipo === 'inquilino') {
            $modelo = Inquilino::findOrFail($request->modelo_id);
        }

        $foto = new Foto();
        $foto->url = $request->url;
        $modelo->fotos()->save($foto);

        return redirect()->route('fotos.create')->with('success', 'Foto creada exitosamente');
    }

    public function destroy($id)
    {
        $foto = Foto::findOrFail($id);
        $foto->delete();
        return redirect()->route('fotos.index')->with('success', 'Foto eliminada exitosamente');
    }
}
```
