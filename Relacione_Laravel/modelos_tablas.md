# Modelos

## Alquiler.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alquiler extends Model
{
    use HasFactory;

    protected $fillable = ['monto', 'fecha_inicio', 'fecha_fin'];

    public function inquilinos()
    {
        return $this->belongsToMany(Inquilino::class);
    }

    public function rentas()
    {
        return $this->hasMany(Renta::class);
    }
}

```

## Departamento.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    use HasFactory;

    protected $fillable = ['direccion', 'renta', 'propietario_id'];

    public function propietario()
    {
        return $this->belongsTo(Propietario::class);
    }

    public function rentas()
    {
        return $this->hasMany(Renta::class);
    }

    public function fotos()
    {
        return $this->morphMany(Foto::class, 'imageable');
    }
}
```

## Foto.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    use HasFactory;

    protected $fillable = ['url', 'imageable_id', 'imageable_type'];

    public function imageable()
    {
        return $this->morphTo();
    }
}
```

## Inquilino.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inquilino extends Model
{
    use HasFactory;

    protected $fillable = ['nombre', 'correo_electronico'];

    public function alquileres()
    {
        return $this->belongsToMany(Alquiler::class);
    }

    public function servicios()
    {
        return $this->belongsToMany(Servicio::class, 'inquilino_servicio');
    }

    public function rentas()
    {
        return $this->hasMany(Renta::class);
    }

    public function fotos()
    {
        return $this->morphMany(Foto::class, 'imageable');
    }
}
```

## InquilinoAlquiler.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//class InquilinoAlquiler extends Model
class InquilinoAlquiler extends Pivot
{
    //use HasFactory;

    protected $table = 'inquilino_alquiler';
}
```

## InquilinoServicio.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

//class InquilinoServicio extends Model
class InquilinoServicio extends Pivot
{
    //use HasFactory;
    protected $table = 'inquilino_servicio';
}
```

## Pago.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    use HasFactory;

    protected $fillable = ['renta_id', 'usuario_id', 'monto', 'fecha_pago'];

    public function renta()
    {
        return $this->belongsTo(Renta::class);
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }
}
```

## Pais.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    use HasFactory;

    protected $fillable = ['nombre'];

    protected $table = 'paises';


    public function usuarios()
    {
        return $this->hasMany(Usuario::class);
    }
}
```

## Perfil.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    use HasFactory;

    protected $fillable = ['bio', 'usuario_id'];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }
}
```

## Propietario.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propietario extends Model
{
    use HasFactory;

    protected $fillable = ['nombre', 'email'];

    public function departamentos()
    {
        return $this->hasMany(Departamento::class);
    }
}
```

## Renta.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Renta extends Model
{
    use HasFactory;

    protected $fillable = ['inquilino_id', 'departamento_id', 'monto', 'fecha_pago'];

    public function inquilino()
    {
        return $this->belongsTo(Inquilino::class);
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class);
    }

    public function pagos()
    {
        return $this->hasMany(Pago::class);
    }
}
```

## Servicio.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    use HasFactory;

    protected $fillable = ['nombre', 'precio'];

    public function inquilinos()
    {
        return $this->belongsToMany(Inquilino::class, 'inquilino_servicio');
    }
}
```

## Usuario.php

```php:
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    protected $fillable = ['nombre', 'email', 'pais_id'];

    public function pais()
    {
        return $this->belongsTo(Pais::class);
    }

    public function perfil()
    {
        return $this->hasOne(Perfil::class);
    }

    public function pagos()
    {
        return $this->hasMany(Pago::class);
    }

    public function fotos()
    {
        return $this->morphMany(Foto::class, 'imageable');
    }
}
```
